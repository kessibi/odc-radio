# Shell Version of the ODC Radio

## Explanation

Why not.

It will fetch the audio and play it. Upon SIGINT, the temporary files are to be
deleted. If desired, the current songs will be displayed. 

## How to use

Simply run `./odcradio.sh` for the radio and messages or `./odcradio.sh -q` for
the quiet version.

A man page is readable with `man -l odcradio.l`

## Dependancies

  + `curl` needed for the audio & the API calls
  + `mp213` plays the radio audio

## Useful links

  + [Website](https://odc.live)
  + [Light App](https://app.odc.live)
