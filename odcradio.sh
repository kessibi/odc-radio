#!/bin/bash
#===============================================================================
# HEADER
#===============================================================================
#% NAME
#+    ODC Radio - Play ODC Live's radio
#%
#% SYNOPSIS
#+    ${SCRIPTNAME} [-hq]
#% 
#% DESCRIPTION
#%    This script will play the ODC Live Radio
#% 
#% OPTIONS
#%    -q                      Set the script as quiet, will not output anything
#%                            except on stderr
#%    -h                      Display help
#% 
#% EXAMPLES
#%    ${SCRIPTNAME}
#%    ${SCRIPTNAME} -q
#===============================================================================
# END OF HEADER
#===============================================================================

function raler() {
  echo $1 1>&2
  exit 1
}

function endradio() {
  rm /tmp/ODC_radio
  
  if [ -f /tmp/ODC_icecast ]; then
    rm /tmp/ODC_icecast
  fi

  kill -TERM $CURL_PID
  exit $1
}

function getshow() {
  API=$(curl -s https://airtime.odc.live/api/live-info/?callback)

  TYPE=$(echo $API | grep -Po '"type":.*?[^\\]"' | 
    awk -F\" '{print $4}' | head -2 | tail -1)
  
  if [ $TYPE = "livestream" ]; then
  
    curl -s https://ice.odc.live/status.xsl > /tmp/ODC_icecast
    LIVE=$(cat /tmp/ODC_icecast | grep "Current Song" -A1 | tail -n 1 |
      awk -F\> '{print $2}' | awk -F\< '{print $1}')
    LIVE=${LIVE/\\u00e9/é}
    LIVE=${LIVE/&amp;/&}
    echo -e "${red} • ${color_off}${ul_on}Livestream:${ul_off} ${LIVE}"
    DATE=$((15*60)) 
  else

    TITLE=$(echo $API | grep -Po '"track_title":.*?[^\\]"' |
      awk -F\" '{print $4}' | head -2 | tail -1)

    ARTIST=$(echo $API | grep -Po '"artist_name":.*?[^\\]"' | 
      awk -F\" '{print $4}' | head -2 | tail -1)

    DATE=$(echo $API | grep -Po '"ends":.*?[^\\]"' | 
      awk -F\" '{print $4}' | head -2 | tail -1)
    
    END=$(date -d "$DATE" +'%-M')
    END=$(($END * 60 + $(date -d "$DATE" +'%-S')))
    NOW=$(date +'%-M')
    NOW=$(($NOW * 60 + $(date +'%-S')))
    
    DATE=$(($END - $NOW + 5))
    
    MSG="$(date +"%H:%M"): $ARTIST - $TITLE"
    MSG=${MSG/\\u00e9/é}
    MSG=${MSG/\\u00fc/ü}
    MSG=${MSG/&amp;/&}
    MSG=${MSG/&\#039;/\'}
    echo " $MSG"
  fi

    sleep $DATE
  getshow
}

QUIET=0
while getopts "qh" opt; do
  case $opt in
    q)
      QUIET=1
    ;;
    h)
      beg_header=$(($(grep -n "^# HEADER" $0 | cut -f1 -d:) + 1))
      end_header=$(($(grep -n "^# END OF HEADER" $0 | cut -f1 -d:) - 2))
      head -${end_header} $0 | tail -n $((${end_header} - ${beg_header})) |
        sed -e "s/[#=%+]/ /g" -e "s/\${SCRIPTNAME}/$(basename $0)/g"
      exit 0
    ;;
    \?)
      raler "usage: $0 [-qh]"
    ;;
  esac
done


# test args and dependancies before doing anything
if [ $# -gt 1 ]; then
  raler "usage: $0 [-qh]"
elif ! which curl >/dev/null; then
  raler "you need to install curl to run the radio"
elif ! which mpg123 >/dev/null; then
  raler "you need to install mpg123 (mp3 decoder) to run the radio"
fi

# on SIGINT receive: endradio with exit 0
trap 'endradio 0' 2

ul_on='\e[4m'
ul_off='\e[0m'
color_off='\033[0m'
purple='\033[0;35m'
red='\033[0;31m'

if [ $QUIET -eq 0 ]; then
echo -e "${purple}ODC Live Radio:${color_off}"
fi

curl -s https://ice.odc.live/_a --output /tmp/ODC_radio &
CURL_PID=$!

if [ $QUIET -eq 0 ]; then
  getshow&
fi

sleep 1

mpg123 -q /tmp/ODC_radio
endradio $?

