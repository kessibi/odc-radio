.TH odcradio 7

.SH NAME

odcradio

.SH SYNOPSIS
\fBodcradio, odclive\fP -- Listen to ODC Live's radio

.SH DESCRIPTION
odcradio is the CLI to listen to the ODC Live's radio.
pace
A list of items with descriptions:
.EL
.PP

Here is a list of flags and their descriptions:
.RS
.IP \fB-q
Will turn the radio silent, thus not writing anything on stdout.
.IP \fB-h
Display some help on how to run the radio.
.RE

.SH FILES
.RS
.IP \fB/tmp/ODC_radio\fP
Stores the mp3 received from the server. This file is deleted when the program
ends.
.IP \fB/tmp/ODC_icecast\fP
Stores the response of the icecast server. If there is a live it will be
printed.
.RE
